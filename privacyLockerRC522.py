#!/usr/bin/env python
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setup(26,GPIO.OUT)
GPIO.setup(19,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(4, GPIO.OUT)
buzzer = GPIO.PWM(4, 6000)
code = ''
GPIO.setwarnings(False)    
reader = SimpleMFRC522()
while True:
        try:
            print("Place tag in front of the reader and wait.")
            id, text = reader.read()
            print(id)
            print(text)
            sleep(2)

            if id==906711337202:
                print ("Master User")
                GPIO.output(24,GPIO.HIGH)
                time.sleep(3)
                GPIO.output(24,GPIO.LOW)
                print("Unlocking Door.")
                GPIO.output(26,GPIO.HIGH)
                time.sleep(5)
                print("Locking Door Again.")
                GPIO.output(26,GPIO.LOW)
            else:
                print ("Visitor User")
                GPIO.output(19,GPIO.HIGH)
                time.sleep(3)
                GPIO.output(19,GPIO.LOW)
        except:
                GPIO.cleanup()
