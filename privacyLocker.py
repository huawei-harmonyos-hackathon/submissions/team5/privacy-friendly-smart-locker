#!/usr/bin/env python3
from evdev import InputDevice
from select import select
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(26,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(4, GPIO.OUT)
buzzer = GPIO.PWM(4, 6000)
code = ''

rfid_presented = ""
keys = "X^1234567890XXXXqinhitialiyaqertuibcnmXXXXXXXXXXXXXXXXXXXXXXX"
dev = InputDevice('/dev/input/event0')
while True:
        r,w,x = select([dev], [], [])
        for event in dev.read():
                if event.type==1 and event.value==1:
                        if event.code==28:
                                if rfid_presented=="0010742768":
                                        print("Master User")
                                        GPIO.output(24,GPIO.HIGH)
                                        time.sleep(2)
                                        GPIO.output(24,GPIO.LOW)
                                        # Unlock Door
                                        print("Unlocking Door.")
                                        GPIO.output(26,GPIO.HIGH)
                                        time.sleep(5)
                                        # Lock Door again
                                        print("Locking Door Again.")
                                        GPIO.output(26,GPIO.LOW)
                                else:
                                    print("Visitor")
                                    code = input("Enter visitor code: ")
                                    if code == '12345':
                                        print ("Code Correct")
                                        GPIO.output(24,GPIO.HIGH)
                                        time.sleep(2)
                                        GPIO.output(24,GPIO.LOW)
                                        print("Unlocking Door.")
                                        GPIO.output(26,GPIO.HIGH)
                                        time.sleep(5)
                                        # Lock Door again
                                        print("Locking Door Again.")
                                        GPIO.output(26,GPIO.LOW)
                                    else:
                                        print ("Code incorrect")
                                        GPIO.output(22,GPIO.HIGH)
                                        time.sleep(2)
                                        buzzer.start(5)
                                        time.sleep(1)
                                        print ("LRed ED off")
                                        GPIO.output(22,GPIO.LOW)
                                        print("Access Denied.")
                                rfid_presented = ""        
                        else:
                                rfid_presented += keys[ event.code ]
        
