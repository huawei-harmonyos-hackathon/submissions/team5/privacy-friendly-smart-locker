# Privacy Friendly Smart Locker

This solution aims to build a privacy friendly locker which can be locally accessed by via RFID tags (2FA) ,as well as connected and controlled via local dashboard in emergency cases.

Regular use is done via RFID Tags assigned according the different user groups
Emergency Use is activated by pressing a hidden button on the hardware which enables a dashboard (only local access)

![Architecture](./Concept_Smart_Locker.png)

### Code files

**privacyLocker**  code for USB RFID reader 

**privacyLockerRC522** code for Mini RC22 reader

**interruptButton** code to be implemented



## Component List - What you will need to build your own

-	1 x Raspberry Pi 3 + Case 
-	1 x Raspberry Pi 3 Pi 2 Pi Model B+ GPIO Expansion (Model B used for this tutorial)
-	1 x 4inch Raspberry Pi LCD Display,480x320 Hardware Resolution TFT Resistive Touch Screen
-	1 x RFID USB Reader + White Card and/or Key Tags
-	1 x Electric Strike Lock (Fail Safe)
-	2 x LEDs 
-	1x Buzzer
-	1 x Push button
-	1 x Power Supply 5 VDC 
-	1 x Power Supply 12V DC
-	Multicolored Wire 40pin Male to Female
-	GPIO Cables for Raspberry Pi Male to Female 40pin Ribbon Wire 

## Raspberry Pi Model 3B pinout setup and wiring Scheme

![WireDiagram](./Wiring_Diagram.png)

### General components

- LED_RED       PIN22
- LED_BLUE      PIN24
- Buzzer        PIN21
- Push button   PIN27
- 5V Relay      PIN26

### RFID Reader Options

1. RFID Reader connected directly to RaspberryPi USB
_Important - You need to identify in which USB port your RFID Reader is connected and change it on the code.
The following command will list the USB devices, check wether you reader is in port 0 or 1._ 

`ls /dev/ttyUSB*`


2. Mini RFID RC522 
- SDA           CEO (PIN23)
- SCL           SCLK (PIN24)
- MOSI          PIN19
- MISO          PIN21
- GND           GND (PIN6)
- RST           PIN25
- 3V3           3.3V (PIN1)


![RFID](./Wiring_Diagram_RFID.png)



## Node-Red Flow

Import the flow to your Node-Red GUI in order for modfying and changing parameters

![Node-RED](./node-red_flow.png)

`
[{"id":"90bd19e5.bb53d8","type":"tab","label":"Smart Locker - Privacy Friendly","disabled":false,"info":""},{"id":"6b76782e.f054e8","type":"inject","z":"90bd19e5.bb53d8","name":"Button","topic":"","payload":"1","payloadType":"str","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":190,"y":80,"wires":[["2d1fee08.a09452"]]},{"id":"3bc0efd2.c7e29","type":"mqtt in","z":"90bd19e5.bb53d8","name":"","topic":"pi/button","qos":"0","datatype":"auto","broker":"ba37f67b.3c2028","x":180,"y":160,"wires":[["fdde10cb.cdcbf","1a77ac5b.b545b4"]],"info":"Emergency Button has been pressed"},{"id":"2d1fee08.a09452","type":"mqtt out","z":"90bd19e5.bb53d8","name":"","topic":"pi/button","qos":"","retain":"","broker":"ba37f67b.3c2028","x":500,"y":80,"wires":[]},{"id":"fdde10cb.cdcbf","type":"debug","z":"90bd19e5.bb53d8","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"payload","targetType":"msg","x":440,"y":160,"wires":[]},{"id":"2926e5bc.b9494a","type":"rpi-gpio in","z":"90bd19e5.bb53d8","name":"Door Opens","pin":"26","intype":"tri","debounce":"25","read":true,"x":330,"y":360,"wires":[["10af1c96.36c4a3"]]},{"id":"e60defd9.ec55e","type":"rpi-gpio in","z":"90bd19e5.bb53d8","name":"Door Closes","pin":"26","intype":"tri","debounce":"25","read":false,"x":330,"y":460,"wires":[["10af1c96.36c4a3"]]},{"id":"10af1c96.36c4a3","type":"ui_switch","z":"90bd19e5.bb53d8","name":"Door Locker","label":"Door Locker","tooltip":"","group":"119d2e45.69f932","order":2,"width":"4","height":"3","passthru":false,"decouple":"false","topic":"","style":"","onvalue":"1","onvalueType":"str","onicon":"","oncolor":"blue","offvalue":"0","offvalueType":"str","officon":"","offcolor":"gray","x":550,"y":420,"wires":[["618dc5b6.b311ac","efe87717.1f1d98","fdc8edb.0026a1"]]},{"id":"1a77ac5b.b545b4","type":"ui_toast","z":"90bd19e5.bb53d8","position":"top right","displayTime":"3","highlight":"","sendall":true,"outputs":0,"ok":"OK","cancel":"","topic":"pi/button","name":"Button pushed","x":440,"y":200,"wires":[],"info":"Emergency button Active\nWait for instructions"},{"id":"618dc5b6.b311ac","type":"mqtt out","z":"90bd19e5.bb53d8","name":"","topic":"pi/lock","qos":"","retain":"","broker":"ba37f67b.3c2028","x":750,"y":260,"wires":[]},{"id":"298fd33.08ba22c","type":"mqtt in","z":"90bd19e5.bb53d8","name":"","topic":"pi/lock","qos":"0","datatype":"auto","broker":"ba37f67b.3c2028","x":150,"y":520,"wires":[["f10415f6.bb39e8"]],"info":"Emergency Button has been pressed"},{"id":"10f1e1d6.2ad8ce","type":"comment","z":"90bd19e5.bb53d8","name":"Dashboard Available - Password need","info":"","x":570,"y":40,"wires":[]},{"id":"f10415f6.bb39e8","type":"debug","z":"90bd19e5.bb53d8","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"payload","targetType":"msg","x":330,"y":580,"wires":[]},{"id":"efe87717.1f1d98","type":"rpi-gpio out","z":"90bd19e5.bb53d8","name":"LED ON","pin":"18","set":true,"level":"0","freq":"","out":"out","x":800,"y":460,"wires":[]},{"id":"fdc8edb.0026a1","type":"rpi-gpio out","z":"90bd19e5.bb53d8","name":"LED OFF","pin":"18","set":true,"level":"0","freq":"","out":"out","x":800,"y":400,"wires":[]},{"id":"ba37f67b.3c2028","type":"mqtt-broker","z":"","name":"","broker":"localhost","port":"1883","clientid":"","usetls":false,"compatmode":true,"keepalive":"60","cleansession":true,"birthTopic":"","birthQos":"0","birthPayload":"","closeTopic":"","closeQos":"0","closePayload":"","willTopic":"","willQos":"0","willPayload":""},{"id":"119d2e45.69f932","type":"ui_group","z":"","name":"Locker Remote Control","tab":"370076b1.27576a","disp":true,"width":"6","collapse":false},{"id":"370076b1.27576a","type":"ui_tab","z":"","name":"Smart Locker App","icon":"dashboard","disabled":false,"hidden":false}]`




