#!/usr/bin/env python3.0 
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)  
raw_input= " "

GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)   
raw_input= input("Press Enter when ready")  

print ("Waiting for falling edge on port 27")  
print ("During this waiting time, your computer is not")   
print ("wasting resources by polling for a button press.\n")  
print ("Press your button when ready to initiate a falling edge interrupt.")  
try:  
    GPIO.wait_for_edge(27, GPIO.FALLING)  
    print ("\nFalling edge detected. Now your program can continue with")  
    print ("whatever was waiting for a button press.")  
except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
GPIO.cleanup()           # clean up GPIO on normal exit  

